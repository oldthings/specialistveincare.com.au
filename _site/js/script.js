$(document).ready(function() {
	
	$("#menu-search").submit( function(){
		
            var searchQuery = $("#searchQuery").attr("value");
		
	    document.location = "/search?q=" + searchQuery;
	
            return false;
	} );
	
 	initSliderBox();
});

/* Author: */

function initSliderBox() {
	
	$(".sliderBox").each(function(i){
		
		var sliderBox = $(this);
		var container = sliderBox.find( ".container" );
		var panels = container.find( ".panel" );
		var controller = $( "#" + sliderBox.attr("controllerId") );
		
		var interval;
		var currentIndex = 0;
		
		function changePanel( indx ){
			
			currentIndex = indx % panels.length;			
			container.css({ "margin-left":(-panels.first().outerWidth() * currentIndex) + "px" });
			
			container.find( "a.pinkButton" ).fadeOut("fast").delay( 1500 ).fadeIn("fast");
			
		}
		
		if ( panels.length > 1 ) {
			var controllerLinks = "";
			for (var i=0; i < panels.length; i++) {			
				controllerLinks += "<a href='#' class='sliderLink' >" + i + "</a>";			
			};	
			controller.html( controllerLinks );
			controller.find("a").first().addClass("selected");

			controller.find("a").each(function(i){			
				$(this).click(function(e){
					e.preventDefault();
					
					changePanel( i );
					
					controller.find("a").removeClass("selected");
					$(this).addClass("selected");
					
					if ( interval ) {
						clearInterval( interval );
						interval = null;
					};
				});		
			});
			
			interval = setInterval( function(){
				
				changePanel( currentIndex + 1 );
				
				controller.find("a").removeClass("selected");
				controller.find("a").eq(currentIndex).addClass("selected");
				
				
			}, 10000 );
			
		};
		
		container.css({"width":sliderBox.outerWidth() * (panels.length+1) + "px" });
		
	});
	
}

function initTabBox() {
	
	$(".tabBox").each(function(e){
		var tabbox = $(this);		
		var buttons = tabbox.find(".tabNav ul a");
		var tabContent = tabbox.find(".tabContent");
		
		buttons.each(function(i){
			if ( $(this).hasClass("selected") ) {
				tabbox.find(".tabContent").eq( i ).addClass("show");
			}
		});
		
		buttons.click(function(e){
			e.preventDefault();
			
			buttons.removeClass("selected");
			$(this).addClass("selected");
			
			tabContent.removeClass("show");
			tabContent.eq( $(this).parent().index() ).addClass("show");
			
		});		
	});
	
}

$(function() {    // Makes sure the code contained doesn't run until
                  //     all the DOM elements have loaded
    $('#statefilter').change(function(){
        $('.workshopwrapper').hide();
        $('#' + $(this).val()).show();
    });
});



